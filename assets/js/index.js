
$(window).scroll(function(){
    var scroll = $(window).scrollTop();
    if(scroll < 50){
        $('.km-navbar').removeClass('navbar-on-scroll');
    } else{
        $('.km-navbar').addClass('navbar-on-scroll');
    }
});

$('.view-more-btn').on('click', function () {
    var text_box_col = $(this).parent().parent().toggleClass('col-md-12 order-lg-2 text-center px-lg-0 mb-30');
    var image_box = $(this).parent().parent().next().toggleClass("order-lg-1 offset-lg-3 my-lg-5");
    var main_image = $('km-text-box-card-collapse-img').toggleClass("d-block");
    var km_text_box_odd = $('.km-text-box[data-position="odd-column"]').toggleClass("odd w-100");
    var km_text_box_even = $('.km-text-box[data-position="even-column"]').toggleClass("even w-100");
})